package com.chronomatron.protobufrxservice;

import com.google.common.base.CaseFormat;
import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.compiler.PluginProtos;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

public class RxProtobufServicePlugin {

    private final Set<String> filesToGenerate;
    private final NameResolver nameResolver;
    private final PluginProtos.CodeGeneratorRequest request;

    private RxProtobufServicePlugin(PluginProtos.CodeGeneratorRequest request) {
        this.request = request;
        filesToGenerate = new HashSet<>(request.getFileToGenerateList());
        nameResolver = new NameResolver(request.getProtoFileList());
    }

    public static void main(String[] args) throws Exception {
        PluginProtos.CodeGeneratorRequest request = PluginProtos.CodeGeneratorRequest.parseFrom(System.in);
        new RxProtobufServicePlugin(request).generate().writeTo(System.out);
    }

    private PluginProtos.CodeGeneratorResponse generate() throws Exception {
        PluginProtos.CodeGeneratorResponse.Builder response = PluginProtos.CodeGeneratorResponse.newBuilder();

        request.getProtoFileList()
               .stream()
               .filter(f -> filesToGenerate.contains(f.getName()))
               .forEach(protoFile -> {
                   String services = protoFile.getServiceList()
                                              .stream()
                                              .map(this::generateServiceInterface)
                                              .collect(Collectors.joining("\n\n"));
                   if (!services.isEmpty()) {
                       response.addFileBuilder()
                               .setName(getSourceFileName(protoFile, getOuterClassName(protoFile)))
                               .setInsertionPoint("outer_class_scope")
                               .setContent(services);

                   }
               });

        return response.build();
    }

    private static String getSourceFileName(DescriptorProtos.FileDescriptorProto protoFile, String outerClassName) {
        return getJavaPackageName(protoFile).replace(".", "/") + "/" + outerClassName + ".java";
    }

    private static String getJavaPackageName(DescriptorProtos.FileDescriptorProto protoFile) {
        if (protoFile.getOptions().hasJavaPackage()) {
            return protoFile.getOptions().getJavaPackage();
        }
        return protoFile.getPackage();
    }


    private static String getOuterClassName(DescriptorProtos.FileDescriptorProto protoFile) {
        if (protoFile.getOptions().hasJavaOuterClassname()) {
            return protoFile.getOptions().getJavaOuterClassname();
        }
        String protoFileName = protoFile.getName();
        String protoFileBaseName = protoFileName.endsWith(".proto") ? protoFileName.substring(0, protoFileName.length() - ".proto".length()) : protoFileName;
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, protoFileBaseName);
    }

    private String generateServiceInterface(DescriptorProtos.ServiceDescriptorProto service) {
        return printToString(out -> {
            String serviceName = service.getName();

            out.printf("public interface %s {\n", serviceName);
            for (DescriptorProtos.MethodDescriptorProto method : service.getMethodList()) {
                out.printf("  rx.Observable<%s> %s(%s request);\n",
                        nameResolver.resolve(method.getOutputType()),
                        method.getName(),
                        nameResolver.resolve(method.getInputType())
                );
            }

            out.println("}\n");
        });
    }

    private static class NameResolver {
        private Map<String, String> proto2JavaNames = new HashMap<>();

        NameResolver(List<DescriptorProtos.FileDescriptorProto> files) {
            for (DescriptorProtos.FileDescriptorProto file : files) {
                String outerProtoName = "." + getJavaPackageName(file);
                String outerJavaName = getJavaPackageName(file) + "." + getOuterClassName(file);

                for (DescriptorProtos.DescriptorProto message : file.getMessageTypeList()) {
                    resolveMessage(outerProtoName, outerJavaName, message);
                }

                for (DescriptorProtos.EnumDescriptorProto e : file.getEnumTypeList()) {
                    resolveEnum(outerProtoName, outerJavaName, e);
                }
            }
        }

        private void resolveEnum(String outerProtoName, String outerJavaName, DescriptorProtos.EnumDescriptorProto e) {
            proto2JavaNames.put(outerProtoName + "." + e.getName(), outerJavaName + "." + e.getName());
        }

        private void resolveMessage(String outerProtoName, String outerJavaName, DescriptorProtos.DescriptorProto message) {
            String javaName = outerJavaName + "." + message.getName();
            String protoName = outerProtoName + "." + message.getName();
            proto2JavaNames.put(protoName, javaName);

            for (DescriptorProtos.EnumDescriptorProto e : message.getEnumTypeList()) {
                resolveEnum(protoName, javaName, e);
            }

            for (DescriptorProtos.DescriptorProto proto : message.getNestedTypeList()) {
                resolveMessage(protoName, javaName, proto);
            }
        }

        String resolve(String protoType) {
            return checkNotNull(proto2JavaNames.get(protoType), "Can't resolve %s in %s", protoType, proto2JavaNames);
        }
    }

    private static String printToString(ThrowableConsumer<PrintWriter> consumer) {
        try (StringWriter stringWriter = new StringWriter()) {
            try (PrintWriter out = new PrintWriter(stringWriter)) {
                consumer.accept(out);
            }

            return stringWriter.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    interface ThrowableConsumer<T> {
        void accept(T t) throws Exception;
    }
}

package com.chronomatron.protobufrxservice;

import org.gradle.api.Plugin
import org.gradle.api.Project

class ProtobufRxServiceGradlePlugin implements Plugin<Project> {

    public static
    final String PROPERTIES_FILE = "META-INF/gradle-plugins/com.chronomatron.protobuf-rxservice.properties"

    void apply(Project project) {
        def properties = new Properties()
        properties.load(getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE))

        project.protobuf {
            plugins {
                // register plugin.
                rxservices {
                    artifact = 'com.chronomatron.protobuf-rxservice:protoc-plugin:' + properties.getProperty('version')
                }
            }

            generateProtoTasks {
                all().each { task ->
                    task.plugins {
                        // enable plugin.
                        rxservices {
                            outputSubDir = 'java'
                        }
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        return "ProtobufRxServiceGradlePlugin{}";
    }
}
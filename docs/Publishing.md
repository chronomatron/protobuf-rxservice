Publishing New Version
======================

- `git tag -a <version> -m "<msg>"` - tag new version
- `./gradlew printVersion` - check gradle version
- `./gradlew clean bintrayUpload` - **clean** build and upload.
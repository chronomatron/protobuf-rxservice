Protobuf-RxService Plugin
===========================

[![Buildstatus](https://pipelines-badges-service.useast.staging.atlassian.io/badge/chronomatron/protobuf-rxservice.svg)](https://bitbucket.org/chronomatron/protobuf-rxservice/addon/pipelines/home#!/)
[![Bintray](https://img.shields.io/bintray/v/chronomatron/maven/com.chronomatron.protobuf-rxservice.svg?maxAge=3600)](https://bintray.com/chronomatron/maven/com.chronomatron.protobuf-rxservice/_latestVersion)

protobuf-rxservice is the plugin for Gradle and [Google protobufs](https://github.com/google/protobuf) that generates
[RxJava](https://github.com/ReactiveX/RxJava) interfaces for services.

Usage
=====

Register plugin in `build.gradle`:

    buildscript {
        repositories {
            jcenter()
        }
        dependencies {
            classpath 'com.chronomatron.protobuf-rxservice:gradle-plugin:<version>'
        }
    }

    apply plugin: 'com.chronomatron.protobuf-rxservice'

Generated Code
==============

For each service in `.proto` file:

    service SearchService {
      rpc Search (SearchRequest) returns (SearchResponse);
    }


an interface is generated in the containing class:

    public interface SearchService {
      rx.Observable<SearchResponse> Search(SearchRequest request);
    }